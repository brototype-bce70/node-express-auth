var express = require('express');
const { response } = require('../app');
var router = express.Router();
var userHelper = require('../helpers/user-helpers')


/* GET users listing. */
router.get('/', function(req, res) {
  let currUser = req.session.user
  // console.log(currUser);
  res.render('user/root',{user: true, currUser})
});

router.get('/login', function(req, res) {
  if(req.session.loggedIn){
    res.redirect('/')
  }else{
    res.render('user/userIndex')
  }
});


router.post('/login-action', (req,res) =>{

  userHelper.doLogin(req.body).then((response)=>{
    if(response.status){
      req.session.loggedIn=true
      req.session.user= response.user
  
      res.redirect('/')
    }else{
      res.redirect('/login')
    }
  })
})

router.get('/signup', (req, res) => {
  if(req.session.loggedIn){
    res.redirect('/')
  }else{
    res.render('user/signup')
  }
})

router.post('/signup-action', (req,res) =>{
  
  userHelper.doSignUp(req.body).then((response)=>{
    console.log(response);
  })
  res.redirect("/")
})

router.get('/logout', (req,res)=>{
  req.session.destroy()
  res.redirect('/')
})


module.exports = router;
