var express = require('express');
const { response } = require('../app');
var router = express.Router();
var adminHelper=require('../helpers/admin-helpers')

/* GET home page. */
router.get('/',validation, function(req, res) {
  adminHelper.showUsers().then((users)=>{
    console.log(users);
    let currUser = req.session.admin
    res.render('admin/adminIndex',{admin: true,currUser,users});
  })
});

router.get('/login',(req,res)=>{
  if(req.session.loggedIn){
    res.redirect('/admin')
  }
  res.render('admin/admin-login')
})

router.post('/login',(req,res)=>{
  // console.log(req.body);
  adminHelper.adminLogin(req.body).then((response)=>{
    if(response.status){
    
      req.session.loggedIn = true
      req.session.admin = response.admin
      res.redirect('/admin/')
    }else{
      
      res.redirect('/admin/login')
    }
  })
})

// router.get('/signup/:username/:password',async(req,res)=>{
//   await adminHelper.addAdmin(req.params.username,req.params.password).then((result)=>{
//     console.log(result);
//   })
//   res.send('admin created')
// })

router.get('/addUser', validation,(req,res)=>{
  res.render('admin/add-user')
})

router.post('/addUser',async(req,res)=>{
  await adminHelper.addUser(req.body)
  res.redirect('/admin/')
})


router.get('/edit/:id', validation, async(req,res)=>{

  let user =await adminHelper.getUser(req.params.id)
  // console.log(user);
  res.render('admin/edit-form',{user: user})
})

router.post('/update',async(req,res)=>{
  await adminHelper.updateUser(req.body.userId, req.body)
  res.redirect('/admin/')
})

router.get('/delete/:id', validation, async(req,res)=>{
  // console.log(req.params.id);
  await adminHelper.deleteUser(req.params.id)
  res.redirect('/admin/')
})

router.get('/logout',(req,res)=>{
  req.session.destroy()
  res.redirect('/admin/login')
})

function validation(req,res,next){
  if(req.session.loggedIn){
    next()
  }else{
    res.redirect('/admin/login')
  }
}

module.exports = router;
