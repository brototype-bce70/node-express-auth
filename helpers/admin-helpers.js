var db = require('../config/connection')
var collection = require('../config/collections')
const bcrypt = require('bcrypt')
const { ObjectID } = require('bson')

module.exports = {

    showUsers: () => {
        return new Promise(async (resolve, reject) => {
            let users = await db.get().collection(collection.USER_COLLECTION).find().toArray()
            resolve(users)
        })
    },

    getUser: (userId) => {
        return new Promise(async (resolve, reject) => {
            let user = await db.get().collection(collection.USER_COLLECTION).findOne({ _id: ObjectID(userId) })
            resolve(user)
        })
    },

    updateUser: (userId, user) => {
        return new Promise((resolve, reject) => {
            db.get().collection(collection.USER_COLLECTION).updateOne({ _id: ObjectID(userId) }, {
                $set: {
                    fullname: user.fname,
                    username: user.username
                }
            })
            resolve()
        })
    },

    deleteUser: (userId) => {
        return new Promise((resolve, reject) => {
            db.get().collection(collection.USER_COLLECTION).deleteOne({ _id: ObjectID(userId) })
            resolve()
        })
    },

    addUser: (user) => {
        return new Promise(async (resolve, reject) => {
            user.password = await bcrypt.hash(user.password, 10)
            db.get().collection(collection.USER_COLLECTION).insertOne(user)
            resolve()
        })
    },

    addAdmin: (userName, password) => {
        return new Promise(async (resolve, reject) => {

            console.log(password);
            password = await bcrypt.hash(password, 10)
            db.get().collection(collection.ADMIN_COLLECTION).insertOne({
                adminUser: userName,
                adminPass: password
            }).then((result) => {
                resolve(result)
            })
        })
    },

    adminLogin: (admin) => {
        return new Promise(async (resolve, reject) => {
            let response = {}
            let superUser = await db.get().collection(collection.ADMIN_COLLECTION).findOne({ adminUser: admin.username })
            if (superUser) {

                bcrypt.compare( admin.password, superUser.adminPass).then((result) => {
                    if (result) {
                        response.status = true
                        response.admin = superUser
                        resolve((response))
                    } else {
                        resolve({ status: false })
                    }
                })
            } else {
                resolve({ status: false })
            }
            //promise end
        })
    }
}